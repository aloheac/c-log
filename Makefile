
CC = gcc
AR = ar
RM = rm


LIB_NAME = c-log
SRC      = src/c-log.c src/override-output.c src/file-utils.c
OBJ      = ${SRC:src/%.c=obj/%.o}
DEP      = $(OBJ:%.o=%.d)
TESTS    = tests/ts-file-utils-001 tests/ts-override-output-001 tests/ts-c-log-001


TARGET     = lib/${LIB_NAME}.a
XPL_TARGET = bin/c-log-example
DIRS       = lib src obj bin


CFLAGS = -Wall -Werror -g

all: ${DIRS} ${TARGET}

tests: ${TESTS}

example: ${XPL_TARGET}

${XPL_TARGET}: ${XPL_TARGET:bin/%=obj/%.o} ${OBJ}
	${CC} -o $@ $^ ${CFLAGS}

${TARGET}: ${OBJ}
	${AR} rcs $@ $^

${DIRS}:
	mkdir -p $@

obj/%.o: src/%.c
	${CC} -c -o $@ $< ${CFLAGS} -MMD

.PHONY: ${TESTS}

${TESTS}:
	${MAKE} -C $@

clean:
	${RM} -f ${TARGET} ${XPL_TARGET} ${OBJ} ${DEP}

-include ${DEP}
