# C-log

Simple library for logs

## c-log examples

See [example](src/c-log-example.c) for example usage.

## How to include c-log in a project

To use C-log in a project, include c-log.h and c-log.c in the project's sources.

It is also possible to link the c-log.a archive, then the c-log.h file must be
in the headers include directories (the compiler's `-I` argument).

## Contribute

### Functionnalities implementation

* See Gitlab's issues to know what needs to be done
* There is headers named `<module>-data.h`. Those contains defines inclined to
  be changed by the user and allows the tests to override some data (once issue
  #18 is fixed).
* TODO's can be used on a branch but not on master. Either they are resolved
  before the merge or an issue must be created.
* Goto's statement can be used for error management (and only for that purpose).
* The `80 characters max` rule must be followed.
* No more than 3 block indentations.
* Each functionnality must be unit-tested.
* Each function must be commented with Doxygen tags
* Tests are code: follow the same rules
* Only one return statement are allowed at the end of the fuction and some
  others when checking input parameters. No others between that

### Tests

The tests are stored in the directory `tests` (that's easy). The sub-directory
name starts with `ts-` for `test-suite`, continues with the module's name and
ends with the test suite's id in a 3 digits number. For example
`tests/ts-c-log-001/`

The tests function's name follow the pattern
`when_stateUnderTest_expect_expectedBehavior`.

A test suite is in it's own directory. It allows (once issue #18 is fixed) to
copy the `*-data.h` headers if necessary and modify it's content. In this way,
we can run tests on smaller or better controlled data sets. In order to test
features with several data set, several test suites must be created.

