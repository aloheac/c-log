#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include "c-log.h"

bool myFunction() {
    int iRes;
    bool fnRes = true;
    char *filePath = "thisFileDoesNotExists.txt";

    logIn();

    logi("Opening an inexisting file");
    iRes = open(filePath, O_RDONLY);
    if (iRes < 0) {
        loge("Failed to open %s (%s)", filePath, strerror(errno));
        return false;
    }

    logi("File opened. If that was possible we should close it now");

    logOut();
    return fnRes;
}

int main() {
    logIn();

    myFunction();

    logOut();
    return EXIT_SUCCESS;
}
