#include "stdlib.h"
#include "stdio.h"
#include "stdarg.h"
#include "c-log.h"
#include "c-log-data.h"

/// Log level's tags
const char * const c_log_levelNames[] = {
    [C_LOG_LVL_ERROR]   = "[ERROR]",
    [C_LOG_LVL_WARNING] = "[ WARN]",
    [C_LOG_LVL_INFO]    = "[ INFO]",
    [C_LOG_LVL_FIRST]   = "[    1]",
    [C_LOG_LVL_SECOND]  = "[    2]",
};

/// Tag printed when the provided log level is invalid
const char *c_log_unknownLevelName = "[UNKWN]";

/**
 * @brief Prints a log
 *
 * @param[in] logLevel     log severity
 * @param[in] functionName name of the function calling this function
 * @param[in] str          format of the log, followed by it's parameters
 */
void c_log(enum c_log_level logLevel,
           const char* functionName,
           const char* str, ...) {

    char        buffer[C_LOG_BUFFER_LEN];
    int         written        = 0;
    const char *logLevelName   = c_log_levelNames[logLevel];
    char       *bufferPtr      = NULL;
    int         remainingBytes = C_LOG_BUFFER_LEN;
    va_list     ap;
    char       *logSeparator   = ": ";
    char       *tagSeparator   = " ";

    if ( (logLevel >= C_LOG_LVL_MAX) || (logLevel < 0) )
    {
        printf("[ERROR] %s: log level %d is not defined\n",
               __FUNCTION__, logLevel);
        logLevelName = c_log_unknownLevelName;
        logLevel     = C_LOG_LVL_ERROR;
    }

    if ( functionName == NULL )
    {
        printf("[ERROR] %s: function's name is not provided\n", __FUNCTION__);
        functionName = "";
        tagSeparator = "";
    }

    if ( str == NULL )
    {
        str          = "";
        logSeparator = "";
    }

    remainingBytes--;           /* keep a spot for the '\n' at the end */
    bufferPtr = buffer;
    written   = snprintf(bufferPtr, remainingBytes, "%s%s%s%s",
                         logLevelName, tagSeparator, functionName,
                         logSeparator);

    if (written < remainingBytes) {
        bufferPtr      += written;
        remainingBytes -= written;

        va_start(ap, str);
        written += vsnprintf(bufferPtr, remainingBytes, str, ap);
        va_end(ap);
    }

    printf("%s\n", buffer);
}

