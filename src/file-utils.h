#ifndef __FILE_UTILS_H__
#define __FILE_UTILS_H__

#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>

bool fileUtils_setCursorPosition(int fd, off_t offset, off_t *previousOffset);

int fileUtils_checkFdContent(int fileFd,
                             const char * const *expectedLines);

int fileUtils_checkFileContent(const char *path,
                               const char * const *expectedLines);

#define IS_FD_VALID(fd) (fd > 0)

#endif
