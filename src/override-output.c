#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "override-output.h"
#include "file-utils.h"

/**
 * @brief Swap the given file descriptors
 *
 * After this function is called, all data written to first fd will be written
 * in the second file. To send data to the first fd, use the file descriptor
 * stored in firstFdCopy
 *
 * @param[out] firstFd  stores the first file descriptor
 * @param[out] secondFd stores the second file descriptor
 *
 * @return the function's status: true or false
 */
bool overrout_overrideOutput(int firstFd, int secondFd, int *firstFdCopy) {
    int  result;
    int  fnResult = false;

    if ( !IS_FD_VALID(firstFd) ) {
        printf("%s: invalid param firstFd: %d\n", __FUNCTION__, firstFd);
        return false;
    }

    if ( !IS_FD_VALID(secondFd) ) {
        printf("%s: invalid param secondFd: %d\n", __FUNCTION__, secondFd);
        return false;
    }

    if ( firstFdCopy == NULL ) {
        printf("%s: param firstFdCopy cannot be NULL\n", __FUNCTION__);
        return false;
    }

    *firstFdCopy = dup(firstFd);
    if (*firstFdCopy < 0) {
        printf("%s: Failed to duplicate first file descriptor (%s)\n",
               __FUNCTION__, strerror(errno));
        goto dup_failed;
    }

    result = dup2(secondFd, firstFd);
    if (result < 0) {
        printf("%s: Failed to replace the first fd with the second fd (%s)\n",
               __FUNCTION__, strerror(errno));
        goto dup2_failed;
    }

    fnResult = true;
    goto success;

dup2_failed:
    result = close(*firstFdCopy);
    if (result != 0) {
        printf("%s: Failed to close duplicate stdout (%s)\n",
               __FUNCTION__, strerror(errno));
    }

dup_failed:
success:
    return fnResult;
}

/**
 * @brief Restore the fd overriden by overrout_overrideOutput
 *
 * This function reverts the overrout_overrideOutput actions: the first fd which
 * was overriden is copied into secondFd and closed and the previously returned
 * copy is set back as first.
 *
 * Note that the function will continue even if errors occurs as it is
 * considered as a cleaning function
 *
 * @param[in]  overridenFd the fd previously known as 'first' and overriden
 * @param[in]  overridenFdCopy the original output copied before being overriden
 * @param[out] secondFd the second fd which overrides the first, copied before
 * being closed
 *
 * @return the function's status: true or false
 */
bool overrout_restoreOverridenFd(int overridenFd,
                                 int overridenFdCopy,
                                 int *secondFd) {
    bool fnResult = true;
    int  result;

    if ( !IS_FD_VALID(overridenFd) ) {
        printf("%s: invalid param overridenFd: %d\n",
               __FUNCTION__, overridenFd);
        return false;
    }

    if ( !IS_FD_VALID(overridenFdCopy) ) {
        printf("%s: invalid param overridenFdCopy: %d\n",
               __FUNCTION__, overridenFdCopy);
        return false;
    }

    if ( secondFd == NULL ) {
        printf("%s: param secondFd cannot be NULL\n", __FUNCTION__);
        return false;
    }

    *secondFd = dup(overridenFd);
    if (*secondFd < 0) {
        printf("%s: Failed to dupplicate fd (%s)\n",
               __FUNCTION__, strerror(errno));
        fnResult = false;
    }

    result = dup2(overridenFdCopy, overridenFd);
    if (result < 0) {
        printf("%s: Failed to restore standard output fd (%s)\n",
               __FUNCTION__, strerror(errno));
        fnResult = false;
    }

    result = close(overridenFdCopy);
    if (result != 0) {
        printf("%s: Failed to close duplicate stdout (%s)\n",
               __FUNCTION__, strerror(errno));
        fnResult = false;
    }

    return fnResult;
}
