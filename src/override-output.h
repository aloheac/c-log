#ifndef __OUTPUT_OVERRIDE_H__
#define __OUTPUT_OVERRIDE_H__

#include <stdbool.h>

bool overrout_overrideOutput(int firstFd, int secondFd, int *firstFdCopy);
bool overrout_restoreOverridenFd(int overridenFd,
                                 int overridenFdCopy,
                                 int *secondFd);

#endif
