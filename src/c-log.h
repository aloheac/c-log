#ifndef __C_LOG_H__
#define __C_LOG_H__

#include <stdbool.h>

enum c_log_level {
    C_LOG_LVL_ERROR,
    C_LOG_LVL_WARNING,
    C_LOG_LVL_INFO,
    C_LOG_LVL_FIRST,
    C_LOG_LVL_SECOND,
    C_LOG_LVL_MAX,     /* Invalid value*/
};

void c_log(enum c_log_level logLevel,
           const char* functionName,
           const char* str, ...);

#define loge(...) c_log(C_LOG_LVL_ERROR,   __FUNCTION__, __VA_ARGS__)
#define logw(...) c_log(C_LOG_LVL_WARNING, __FUNCTION__, __VA_ARGS__)
#define logi(...) c_log(C_LOG_LVL_INFO,    __FUNCTION__, __VA_ARGS__)
#define log1(...) c_log(C_LOG_LVL_FIRST,   __FUNCTION__, __VA_ARGS__)
#define log2(...) c_log(C_LOG_LVL_SECOND,  __FUNCTION__, __VA_ARGS__)

#define logIn() c_log(C_LOG_LVL_SECOND,  __FUNCTION__, "entering function")
#define logOut() c_log(C_LOG_LVL_SECOND, __FUNCTION__, "leaving function")

#endif
