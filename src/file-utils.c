#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include "file-utils.h"

/**
 * @todo Use something more flexible than a define, or raise it's value
 */
#define MAX_OUTPUT_LINE_LENGTH 250

/**
 * @brief Set the file's cursor position and return the previous position
 *
 * @remark \ref previousOffset can be NULL. In this case, the previous position
 * won't be returned
 *
 * @param[in]  fileFd the file's file descriptor
 * @param[in]  offset the wanted cursor position
 * @param[out] previousOffset stores the previous file's cursor position
 *
 * @return the function's status: true or false
 */
bool fileUtils_setCursorPosition(int fd, off_t offset, off_t *previousOffset) {
    bool  fnResult = false;
    off_t oRes;
    off_t tmpOffset;

    if ( !IS_FD_VALID(fd) ) {
        printf("%s: invalid param fd: %d\n", __FUNCTION__, fd);
        return false;
    }

    if ( offset < 0 ) {
        printf("%s: invalid param offset: %ld\n", __FUNCTION__, offset);
        return false;
    }

    tmpOffset = lseek(fd, 0, SEEK_CUR);
    if (tmpOffset < 0) {
        printf("%s: Failed to get file cursor's location (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    oRes = lseek(fd, offset, SEEK_SET);
    if (oRes != offset) {
        printf("%s: Failed set cursor position (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    if (previousOffset != NULL) {
        *previousOffset = tmpOffset;
    }

    fnResult = true;

err:
    return fnResult;
}

/**
 * @brief Compare the file contents with the expected output
 *
 * @remark the file's cursor will be restored in it's current position
 *
 * @param[in] fileFd the file's file descriptor
 * @param[in] expectedLines the file's expected content, line per line and
 * terminating by a NULL value
 *
 * @retval 0 if the contents match
 * @retval 1 if a line differs
 * @retval 2 if the lines corresponds but the file contains more data
 * @retval -1 in case of errors
 */
int fileUtils_checkFdContent(int fileFd,
                             const char * const *expectedLines) {
    int     i;
    int     fnResult = 0;
    char    buffer[MAX_OUTPUT_LINE_LENGTH];
    ssize_t bytesRead;
    off_t   originalOffset;
    size_t  lineLen;
    bool    boolRes;
    int     cmpRes;

    if ( !IS_FD_VALID(fileFd) ) {
        printf("%s: invalid param fileFd: %d\n", __FUNCTION__, fileFd);
        return false;
    }

    if ( expectedLines == NULL ) {
        printf("%s: param expectedLines cannot be NULL\n", __FUNCTION__);
        return false;
    }

    boolRes = fileUtils_setCursorPosition(fileFd, 0, &originalOffset);
    if (boolRes != true) {
        printf("%s: Failed to rewind file cursor\n", __FUNCTION__);
        fnResult = -1;
        goto err;
    }

    i = 0;
    while ( (expectedLines[i] != NULL) && (fnResult == 0) ) {

        lineLen = strlen(expectedLines[i]);
        if (lineLen >= MAX_OUTPUT_LINE_LENGTH) {
            printf("%s: Expected line is too long (max %d)\n",
                   __FUNCTION__, MAX_OUTPUT_LINE_LENGTH);
            fnResult = -1;
            goto reset_cursor;
        }

        bytesRead = read(fileFd, buffer, lineLen);
        if (bytesRead < 0) {
            printf("%s: Failed to read file content (%s)\n",
                   __FUNCTION__, strerror(errno));
            fnResult = -1;
            goto reset_cursor;
        } else if (bytesRead != lineLen) {
            printf("%s: Read %ld bytes from file instead of %ld\n",
                   __FUNCTION__, bytesRead, lineLen);
            fnResult = -1;
            goto reset_cursor;
        }

        buffer[lineLen] = '\0';
        cmpRes = strncmp(expectedLines[i], buffer, MAX_OUTPUT_LINE_LENGTH);
        if (cmpRes != 0) {
            fnResult = 1;
            printf("%s: expected -%s- but read -%s-\n",
                   __FUNCTION__, expectedLines[i], buffer);

        }

        i++;
    }

    if (fnResult == 0) {
        /* Check there is no more data in the file */
        bytesRead = read(fileFd, buffer, MAX_OUTPUT_LINE_LENGTH);
        if (bytesRead > 0) {
            fnResult = 2;
        }
    }

reset_cursor:
    boolRes = fileUtils_setCursorPosition(fileFd, originalOffset, NULL);
    if (boolRes != true) {
        printf("%s: Failed to reset file cursor\n", __FUNCTION__);
        fnResult = -1;
    }

err:
    return fnResult;
}

/**
 * @brief Open the given file and call the ::fileUtils_checkFileContent function
 *
 * @param[in] filePath the file's path
 *
 * See @ref fileUtils_checkFdContent for parameters and return values
 */
int fileUtils_checkFileContent(const char *path,
                               const char * const *expectedLines) {
    int res;
    int fnResult = -1;
    int fileFd;

    if (path == NULL) {
        printf("%s: param path cannot be NULL\n", __FUNCTION__);
        return -1;
    }

    if (expectedLines == NULL) {
        printf("%s: param expectedLines cannot be NULL\n", __FUNCTION__);
        return -1;
    }

    fileFd = open(path, O_RDONLY);
    if (fileFd < 0) {
        printf("%s: failed to open file %s (%s)\n",
               __FUNCTION__, path, strerror(errno));
        goto err;
    }

    fnResult = fileUtils_checkFdContent(fileFd, expectedLines);

    res = close(fileFd);
    if (res != 0) {
        printf("%s: failed to close file %s (%s)\n",
               __FUNCTION__, path, strerror(errno));
        fnResult = -1;
    }

err:
    return fnResult;
}
