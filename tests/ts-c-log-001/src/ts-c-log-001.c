#define _GNU_SOURCE 1
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "override-output.h"
#include "file-utils.h"
#include "c-log.h"
#include "c-log-data.h"

/**
 * @brief Request a log longer than the maximum log length and check that it is
 * cut and the line end's (\n) is present
 *
 * @remark 250 characters is a long log for that kind of test, as soos as the
 * environment allows to override a define this function should be modified with
 * a lower value
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_BufferLenIs250_expect_OutputIs250() {
    int  stdoutCopyFd;
    int  testFileFd;
    bool bRes;
    int  iRes;
    char sentLog[C_LOG_BUFFER_LEN];
    char expectedLog[C_LOG_BUFFER_LEN];
    char tmpDir[]             = "/tmp";
    bool fnRes                = false;
    const char *fileContent[] = {
        NULL, NULL
    };

    fileContent[0] = expectedLog;
    memset(sentLog, 'a', C_LOG_BUFFER_LEN-1);
    sentLog[C_LOG_BUFFER_LEN-1] = '\0';
    strcpy(expectedLog, "[ERROR] fun: ");
    memset(expectedLog + 13, 'a', C_LOG_BUFFER_LEN-14);
    expectedLog[C_LOG_BUFFER_LEN-2] = '\n';
    expectedLog[C_LOG_BUFFER_LEN-1] = '\0';

    testFileFd = open(tmpDir, O_RDWR | O_TMPFILE, S_IRUSR | S_IWUSR);
    if (testFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    bRes = overrout_overrideOutput(STDOUT_FILENO, testFileFd, &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    /* 14 chars for "[ERROR] fun: " (including \0) */
    c_log(C_LOG_LVL_ERROR, "fun", sentLog);

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &testFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    iRes = fileUtils_checkFdContent(testFileFd, fileContent);
    if (iRes != 0) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_err;
    }

    fnRes = true;

override_err:
    iRes = close(testFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }

err:
    return fnRes;
}

/**
 * @brief Request a log with the specified log level and check that the printed
 * log's tag correspond to the given tag
 *
 * @param[in] logLevel the log level
 * @param[in] expected5charsTag the expected tag (5 characters long)
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool check_log_tag(enum c_log_level logLevel,
                          const char *expected5charsTag) {
    int  stdoutCopyFd;
    int  testFileFd;
    bool bRes;
    int  iRes;
    char expectedLog[25];
    char sentLog[]            = "log";
    char tmpDir[]             = "/tmp";
    bool fnRes                = false;
    const char *fileContent[] = {
        expectedLog,
        NULL
    };

    strcpy(expectedLog, "[     ] fun: log\n");
    strncpy(expectedLog+1, expected5charsTag, 5);

    testFileFd = open(tmpDir, O_RDWR | O_TMPFILE, S_IRUSR | S_IWUSR);
    if (testFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    bRes = overrout_overrideOutput(STDOUT_FILENO, testFileFd, &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    c_log(logLevel, "fun", sentLog);

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &testFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    iRes = fileUtils_checkFdContent(testFileFd, fileContent);
    if (iRes != 0) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_err;
    }

    fnRes = true;

override_err:
    iRes = close(testFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }

err:
    return fnRes;
}

/**
 * @brief Request an error log and check that the corresponding tag is printed
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_ErrorLog_expect_TagIsError() {
    bool fnRes = false;

    fnRes = check_log_tag(C_LOG_LVL_ERROR, "ERROR");
    return fnRes;
}

/**
 * @brief Request a warning log and check that the corresponding tag is printed
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_WarningLog_expect_TagIsWarning() {
    bool fnRes = false;

    fnRes = check_log_tag(C_LOG_LVL_WARNING, " WARN");
    return fnRes;
}

/**
 * @brief Request an info log and check that the corresponding tag is printed
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_InfoLog_expect_TagIsInfo() {
    bool fnRes = false;

    fnRes = check_log_tag(C_LOG_LVL_INFO, " INFO");
    return fnRes;
}

/**
 * @brief Request a first level log and check that the corresponding tag is
 * printed
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_L1Log_expect_TagIs1() {
    bool fnRes = false;

    fnRes = check_log_tag(C_LOG_LVL_FIRST, "    1");
    return fnRes;
}

/**
 * @brief Request an second level log and check that the corresponding tag is
 * printed
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_L2Log_expect_TagIs2() {
    bool fnRes = false;

    fnRes = check_log_tag(C_LOG_LVL_SECOND, "    2");
    return fnRes;
}

/**
 * @brief Request a log with an invalid log level and check that an error is
 * printed and the 'unknown' tag is used
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_LevelIsInvalid_expect_Error() {
    int  stdoutCopyFd;
    int  testFileFd;
    bool bRes;
    int  iRes;
    char tmpDir[]             = "/tmp";
    bool fnRes                = false;
    const char *fileContent[] = {
        "[ERROR] c_log: log level 5 is not defined\n",
        "[UNKWN] fun: log\n",
        NULL
    };

    testFileFd = open(tmpDir, O_RDWR | O_TMPFILE, S_IRUSR | S_IWUSR);
    if (testFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    bRes = overrout_overrideOutput(STDOUT_FILENO, testFileFd, &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    c_log(C_LOG_LVL_MAX, "fun", "log");

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &testFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    iRes = fileUtils_checkFdContent(testFileFd, fileContent);
    if (iRes != 0) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_err;
    }

    fnRes = true;

override_err:
    iRes = close(testFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }

err:
    return fnRes;
}

/**
 * @brief Request a log with no function's name and check that an error is
 * printed and the tag and the log are printed anyway
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_FunctionNameIsInvalid_expect_AdaptedMessage() {
    int  stdoutCopyFd;
    int  testFileFd;
    bool bRes;
    int  iRes;
    char tmpDir[]             = "/tmp";
    bool fnRes                = false;
    const char *fileContent[] = {
        "[ERROR] c_log: function's name is not provided\n",
        "[ERROR]: log\n",
        NULL
    };

    testFileFd = open(tmpDir, O_RDWR | O_TMPFILE, S_IRUSR | S_IWUSR);
    if (testFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    bRes = overrout_overrideOutput(STDOUT_FILENO, testFileFd, &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    c_log(C_LOG_LVL_ERROR, NULL, "log");

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &testFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    iRes = fileUtils_checkFdContent(testFileFd, fileContent);
    if (iRes != 0) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_err;
    }

    fnRes = true;

override_err:
    iRes = close(testFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }

err:
    return fnRes;
}

/**
 * @brief Request a log with no log and check that the tag and the function's
 * name are printed without colon between them
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_LogIsInvalid_expect_AdaptedMessage() {
    int  stdoutCopyFd;
    int  testFileFd;
    bool bRes;
    int  iRes;
    char tmpDir[]             = "/tmp";
    bool fnRes                = false;
    const char *fileContent[] = {
        "[ERROR] fun\n",
        NULL
    };

    testFileFd = open(tmpDir, O_RDWR | O_TMPFILE, S_IRUSR | S_IWUSR);
    if (testFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    bRes = overrout_overrideOutput(STDOUT_FILENO, testFileFd, &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    c_log(C_LOG_LVL_ERROR, "fun", NULL);

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &testFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    iRes = fileUtils_checkFdContent(testFileFd, fileContent);
    if (iRes != 0) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_err;
    }

    fnRes = true;

override_err:
    iRes = close(testFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }

err:
    return fnRes;
}

/**
 * @brief This is a function with a specific name that uses every log level's
 * macros without providing variadic parameters (variable number's arguments)
 * and the logIn and the logOut macros as well
 */
static void testMacros() {
    logIn();
    loge("log error");
    logw("log warning");
    logi("log info");
    log1("log first");
    log2("log second");
    logOut();
}

/**
 * @brief This is a function with a specific name that uses every log level's
 * macros providing variadic parameters (variable number's arguments)
 */
static void testMacrosWithArguments() {
    loge("log %s", "error");
    logw("log %s", "warning");
    logi("log %s", "info");
    log1("log %s", "first");
    log2("log %s", "second");
}

/**
 * @brief This functions checks the output of logIn, logOut and every log
 * level's macros without any variadic parameter (variable arguments number)
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_SimpleMacrosAreUsed_expect_OutputIsExact() {
    int  stdoutCopyFd;
    int  testFileFd;
    bool bRes;
    int  iRes;
    char tmpDir[]             = "/tmp";
    bool fnRes                = false;
    const char *fileContent[] = {
        "[    2] testMacros: entering function\n",
        "[ERROR] testMacros: log error\n",
        "[ WARN] testMacros: log warning\n",
        "[ INFO] testMacros: log info\n",
        "[    1] testMacros: log first\n",
        "[    2] testMacros: log second\n",
        "[    2] testMacros: leaving function\n",
        NULL
    };

    testFileFd = open(tmpDir, O_RDWR | O_TMPFILE, S_IRUSR | S_IWUSR);
    if (testFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    bRes = overrout_overrideOutput(STDOUT_FILENO, testFileFd, &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    testMacros();

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &testFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    iRes = fileUtils_checkFdContent(testFileFd, fileContent);
    if (iRes != 0) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_err;
    }

    fnRes = true;

override_err:
    iRes = close(testFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }

err:
    return fnRes;
}

/**
 * @brief This functions checks the output of every log level's macros with
 * variadic parameters (variable arguments number)
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
bool when_MacrosAreUsedWithArgs_expect_OutputIsExact() {
    int  stdoutCopyFd;
    int  testFileFd;
    bool bRes;
    int  iRes;
    char tmpDir[]             = "/tmp";
    bool fnRes                = false;
    const char *fileContent[] = {
        "[ERROR] testMacrosWithArguments: log error\n",
        "[ WARN] testMacrosWithArguments: log warning\n",
        "[ INFO] testMacrosWithArguments: log info\n",
        "[    1] testMacrosWithArguments: log first\n",
        "[    2] testMacrosWithArguments: log second\n",
        NULL
    };

    testFileFd = open(tmpDir, O_RDWR | O_TMPFILE, S_IRUSR | S_IWUSR);
    if (testFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }

    bRes = overrout_overrideOutput(STDOUT_FILENO, testFileFd, &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    testMacrosWithArguments();

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &testFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_err;
    }

    iRes = fileUtils_checkFdContent(testFileFd, fileContent);
    if (iRes != 0) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_err;
    }

    fnRes = true;

override_err:
    iRes = close(testFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }

err:
    return fnRes;
}

/// Array containing all test's functions
bool (*testFunctions[])(void) = {
    when_BufferLenIs250_expect_OutputIs250,
    when_ErrorLog_expect_TagIsError,
    when_WarningLog_expect_TagIsWarning,
    when_InfoLog_expect_TagIsInfo,
    when_L1Log_expect_TagIs1,
    when_L2Log_expect_TagIs2,
    when_LevelIsInvalid_expect_Error,
    when_FunctionNameIsInvalid_expect_AdaptedMessage,
    when_LogIsInvalid_expect_AdaptedMessage,
    when_SimpleMacrosAreUsed_expect_OutputIsExact,
    when_MacrosAreUsedWithArgs_expect_OutputIsExact,
    NULL
};

/**
 * @brief Run all scenarios and check their statuses
 *
 * @retval true if all tests succeeds
 * @retval false if one test fails
 */
int main() {
    int  fnRes = EXIT_SUCCESS;
    bool bRes;
    int  i;

    i = 0;
    while (testFunctions[i] != NULL) {
        printf("Run test #%d\n", i);

        bRes = testFunctions[i]();
        if (bRes == true) {
            printf("Test #%d succeed\n", i);
        } else {
            printf("Test #%d failed !!\n", i);
            fnRes = EXIT_FAILURE;
        }

        i++;
    }

    if (fnRes == EXIT_SUCCESS) {
        printf("Test suite succeed\n");
    } else {
        printf("test suite failed\n");
    }

    return fnRes;
}
