#include "file-utils.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#define TEST_FILE "test-file-001.txt"
#define TEST_EMPTY_FILE "test-file-002.txt"
#define TEST_MISSING_FILE "test-file-000.txt"
#define TEST_ALPHABET_FILE "test-file-003.txt"

/**
 * @brief Move the cursor and call \ref fileUtils_setCursorPosition to check
 * that the previous location returned with the \ref previousOffset param is
 * correct
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_PreviousLocationIsKnown_expect_ReturnedLocationConcur() {
    int     fileFd;
    bool    bRes;
    ssize_t ssRes;
    int     iRes;
    bool    fnRes = false;
    off_t   previousOffset = 0;
    char    buffer[2];

    fileFd = open(TEST_ALPHABET_FILE, O_RDONLY);
    if (fileFd < 0) {
        printf("%s: Failed to open file %s (%s)\n",
               __FUNCTION__, TEST_FILE, strerror(errno));
        goto open_failed;
    }

    ssRes = read(fileFd, &buffer, 2);
    if (ssRes < 0) {
        printf("%s: Failed to read file %s (%s)\n",
               __FUNCTION__, TEST_ALPHABET_FILE, strerror(errno));
        goto test_failed;
    }

    bRes = fileUtils_setCursorPosition(fileFd, 1, &previousOffset);
    if (bRes != true) {
        printf("%s: Failed to set cursor position\n", __FUNCTION__);
        goto test_failed;
    }

    if (previousOffset != 2) {
        printf("%s: Test failed, expected '%d' but got '%ld'\n",
               __FUNCTION__, 2, previousOffset);
        goto test_failed;
    }

    fnRes = true;

test_failed:
    iRes = close(fileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }
    
open_failed:
    return fnRes;
}

/**
 * @brief call \ref fileUtils_setCursorPosition to set the cursor location and
 * check it has been moved
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_LocationIsSet_expect_ReadContentIsRelevant() {
    int     fileFd;
    bool    bRes;
    int     iRes;
    ssize_t ssRes;
    bool    fnRes = false;
    off_t   previousOffset = 0;
    char    letter = ' ';

    fileFd = open(TEST_ALPHABET_FILE, O_RDONLY);
    if (fileFd < 0) {
        printf("%s: Failed to open file %s (%s)\n",
               __FUNCTION__, TEST_FILE, strerror(errno));
        goto open_failed;
    }

    bRes = fileUtils_setCursorPosition(fileFd, 1, &previousOffset);
    if (bRes != true) {
        printf("%s: Failed to set cursor position\n", __FUNCTION__);
        goto test_failed;
    }

    ssRes = read(fileFd, &letter, 1);
    if (ssRes < 0) {
        printf("%s: Failed to read file %s (%s)\n",
               __FUNCTION__, TEST_ALPHABET_FILE, strerror(errno));
        goto test_failed;
    } else if (ssRes != 1) {
        printf("%s: Failed to read file, expected %d but returned %ld\n",
               __FUNCTION__, 1, ssRes);
        goto test_failed;
    }

    if (letter != 'b') {
        printf("%s: Test failed, expected '%c' but got '%c'\n",
               __FUNCTION__, 'b', letter);
        goto test_failed;
    }

    fnRes = true;

test_failed:
    iRes = close(fileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }
    
open_failed:
    return fnRes;
}

/**
 * @brief Open a file with a known content and call \ref
 * fileUtils_checkFdContent with that content as parameter and check that the
 * comparison returns success
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_FileContentIsDefined_expect_ComparisonSucceed() {
    int  fileFd;
    int  iRes;
    bool fnRes = false;
    const char *expectedLines[] = {
        [0] = "First line\n",
        [1] = "Second line\n",
        NULL
    };

    fileFd = open(TEST_FILE, O_RDONLY);
    if (fileFd < 0) {
        printf("%s: Failed to open file %s (%s)\n",
               __FUNCTION__, TEST_FILE, strerror(errno));
        goto open_failed;
    }

    iRes = fileUtils_checkFdContent(fileFd, expectedLines);
    if (iRes != 0) {
        printf("%s: Test expected %d but returned %d\n",
               __FUNCTION__, 0, iRes);
        goto test_failed;
    }

    fnRes = true;

test_failed:
    iRes = close(fileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }
    
open_failed:
    return fnRes;
}

/**
 * @brief Call \ref fileUtils_setCursorPosition without the \ref previousOffset
 * param ad check that no crash occurs and the cursor is successfully moved
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_OutputValueIsNull_expect_FunctionSucceeds() {
    int     fileFd;
    bool    bRes;
    int     iRes;
    ssize_t ssRes;
    bool    fnRes = false;
    char    letter = ' ';

    fileFd = open(TEST_ALPHABET_FILE, O_RDONLY);
    if (fileFd < 0) {
        printf("%s: Failed to open file %s (%s)\n",
               __FUNCTION__, TEST_FILE, strerror(errno));
        goto open_failed;
    }

    bRes = fileUtils_setCursorPosition(fileFd, 1, NULL);
    if (bRes != true) {
        printf("%s: Failed to set cursor position\n", __FUNCTION__);
        goto test_failed;
    }

    ssRes = read(fileFd, &letter, 1);
    if (ssRes < 0) {
        printf("%s: Failed to read file %s (%s)\n",
               __FUNCTION__, TEST_ALPHABET_FILE, strerror(errno));
        goto test_failed;
    } else if (ssRes != 1) {
        printf("%s: Failed to read file, expected %d but returned %ld\n",
               __FUNCTION__, 1, ssRes);
        goto test_failed;
    }

    if (letter != 'b') {
        printf("%s: Test failed, expected '%c' but got '%c'\n",
               __FUNCTION__, 'b', letter);
        goto test_failed;
    }

    fnRes = true;

test_failed:
    iRes = close(fileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }
    
open_failed:
    return fnRes;
}

/**
 * @brief Call \ref fileUtils_checkFdContent with non-matching content and check
 * that the comparison fails
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_ExpectedLinesAreWrong_expect_ComparisonFails() {
    int  fileFd;
    int  iRes;
    bool fnRes = false;
    const char *expectedLines[] = {
        [0] = "First wrong line\n",
        [1] = "Second line\n",
        NULL
    };

    fileFd = open(TEST_FILE, O_RDONLY);
    if (fileFd < 0) {
        printf("%s: Failed to open file %s (%s)\n",
               __FUNCTION__, TEST_FILE, strerror(errno));
        goto open_failed;
    }

    iRes = fileUtils_checkFdContent(fileFd, expectedLines);
    if (iRes != 1) {
        printf("%s: Test expected %d but returned %d\n",
               __FUNCTION__, 1, iRes);
        goto test_failed;
    }

    fnRes = true;

test_failed:
    iRes = close(fileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }
    
open_failed:
    return fnRes;
}

/**
 * @brief Call \ref fileUtils_checkFdContent on an empty file and check we can
 * assert it's content (with an empty array as parameter)
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_FileIsEmpty_expect_ComparisonSucceed() {
    int  fileFd;
    int  iRes;
    bool fnRes = false;
    const char *expectedLines[] = {
        NULL
    };

    fileFd = open(TEST_EMPTY_FILE, O_RDONLY);
    if (fileFd < 0) {
        printf("%s: Failed to open file %s (%s)\n",
               __FUNCTION__, TEST_FILE, strerror(errno));
        goto open_failed;
    }

    iRes = fileUtils_checkFdContent(fileFd, expectedLines);
    if (iRes != 0) {
        printf("%s: Test expected %d but returned %d\n",
               __FUNCTION__, 0, iRes);
        goto test_failed;
    }

    fnRes = true;

test_failed:
    iRes = close(fileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }
    
open_failed:
    return fnRes;
}

/**
 * @brief Call \ref fileUtils_checkFileContent with a known file and check it's
 * comparison succeeds
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_UsingFilePath_expect_ComparisonSucceed() {
    int  iRes;
    bool fnRes = false;
    const char *expectedLines[] = {
        [0] = "First line\n",
        [1] = "Second line\n",
        NULL
    };

    iRes = fileUtils_checkFileContent(TEST_FILE, expectedLines);
    if (iRes != 0) {
        printf("%s: Test expected %d but returned %d\n",
               __FUNCTION__, 0, iRes);
        goto test_failed;
    }

    fnRes = true;

test_failed:
    return fnRes;
}

/**
 * @brief Call \ref fileUtils_checkFileContent on an non-existing file and check
 * that an error is returned
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_FileIsMissing_expect_ErrorIsReturned() {
    int  iRes;
    bool fnRes = false;
    const char *expectedLines[] = {
        [0] = "First line\n",
        [1] = "Second line\n",
        NULL
    };

    iRes = fileUtils_checkFileContent(TEST_MISSING_FILE, expectedLines);
    if (iRes != -1) {
        printf("%s: Test expected %d but returned %d\n",
               __FUNCTION__, -1, iRes);
        goto test_failed;
    }

    fnRes = true;

test_failed:
    return fnRes;
}

/// Array containing all test's functions
bool (*testFunctions[])(void) = {
    when_PreviousLocationIsKnown_expect_ReturnedLocationConcur,
    when_LocationIsSet_expect_ReadContentIsRelevant,
    when_OutputValueIsNull_expect_FunctionSucceeds,
    when_FileContentIsDefined_expect_ComparisonSucceed,
    when_ExpectedLinesAreWrong_expect_ComparisonFails,
    when_FileIsEmpty_expect_ComparisonSucceed,
    when_UsingFilePath_expect_ComparisonSucceed,
    when_FileIsMissing_expect_ErrorIsReturned,
    NULL
};

/**
 * @brief Run all scenarios and check their statuses
 *
 * @retval true if all tests succeeds
 * @retval false if one test fails
 */
int main() {
    int  fnRes = EXIT_SUCCESS;
    bool bRes;
    int  i;

    i = 0;
    while (testFunctions[i] != NULL) {
        printf("Run test #%d\n", i);

        bRes = testFunctions[i]();
        if (bRes == true) {
            printf("Test #%d succeed\n", i);
        } else {
            printf("Test #%d failed !!\n", i);
            fnRes = EXIT_FAILURE;
        }

        i++;
    }

    if (fnRes == EXIT_SUCCESS) {
        printf("Test suite succeed\n");
    } else {
        printf("test suite failed\n");
    }

    return fnRes;
}

