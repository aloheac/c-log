
#define _GNU_SOURCE 1

#include "override-output.h"
#include "file-utils.h"
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

/**
 * @brief Prints data while stdout is overriden and after, then check that
 * everything was written on a temporary file while the output was overriden and
 * not after it was restored
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_OutputIsOverriden_expect_fileContainsAllText() {
    bool  fnResult         = false;
    int   stdoutCopyFd     = 0;
    int   overrideFileFd   = 0;
    bool  bRes             = false;
    char *tmpDir           = "/tmp";
    int   iRes           = 0;
    const char *output[] = {
        [0] = "This is printed on the fake stdout\n",
        [1] = "This is also printed on the fake stdout\n",
        NULL
    };

    overrideFileFd = open(tmpDir,
                          O_RDWR | O_TMPFILE,
                          S_IRUSR | S_IWUSR);
    if (overrideFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }
    
    bRes = overrout_overrideOutput(STDOUT_FILENO,
                                   overrideFileFd,
                                   &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_done;
    }

    printf(output[0]);
    dprintf(STDOUT_FILENO, output[1]);
    dprintf(stdoutCopyFd, "This is printed on the real stdout\n");

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &overrideFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_done;
    }

    printf("Output is back to normal\n");

    iRes = fileUtils_checkFdContent(overrideFileFd, output);
    if (iRes != 0) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_done;
    }

    fnResult = true;

override_done:
    iRes = close(overrideFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }
    
err:
    return fnResult;
}

/**
 * @brief Check that the comparison function doesn't always returns true
 *
 * @remark This tests is already done in the fileUtils's tests so this test
 * should be removed as soon as it bothers us
 *
 * @retval true if the test succeeds
 * @retval false if the test fails
 */
static bool when_ExpectedContentIsWrong_expect_ComarisonFails() {
    bool  fnResult         = false;
    int   stdoutCopyFd     = 0;
    int   overrideFileFd   = 0;
    bool  bRes             = false;
    char *tmpDir           = "/tmp";
    int   iRes           = 0;
    const char *output[] = {
        [0] = "This is printed on the fake stdout\n",
        [1] = "This is also printed on the fake stdout\n",
        NULL
    };
    const char *wrongOutput[] = {
        [0] = "This is not printed on the fake stdout\n",
        [1] = "This is also printed on the fake stdout\n",
        NULL
    };

    overrideFileFd = open(tmpDir,
                          O_RDWR | O_TMPFILE,
                          S_IRUSR | S_IWUSR);
    if (overrideFileFd < 0) {
        printf("%s: Failed to create temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
        goto err;
    }
    
    bRes = overrout_overrideOutput(STDOUT_FILENO,
                                   overrideFileFd,
                                   &stdoutCopyFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_done;
    }

    printf(output[0]);
    dprintf(STDOUT_FILENO, output[1]);
    dprintf(stdoutCopyFd, "This is printed on the real stdout\n");

    bRes = overrout_restoreOverridenFd(STDOUT_FILENO,
                                       stdoutCopyFd,
                                       &overrideFileFd);
    if (bRes != true) {
        printf("%s: Failed to override stdout\n", __FUNCTION__);
        goto override_done;
    }

    printf("Output is back to normal\n");

    iRes = fileUtils_checkFdContent(overrideFileFd, wrongOutput);
    if (iRes != 1) {
        printf("%s: Failed to assert file content (%d)\n", __FUNCTION__, iRes);
        goto override_done;
    }

    fnResult = true;

override_done:
    iRes = close(overrideFileFd);
    if (iRes != 0) {
        printf("%s: Failed to close temporary file (%s)\n",
               __FUNCTION__, strerror(errno));
    }
    
err:
    return fnResult;
}

/// Array containing all test's functions
bool (*testFunctions[])(void) = {
    when_OutputIsOverriden_expect_fileContainsAllText,
    when_ExpectedContentIsWrong_expect_ComarisonFails,
    NULL
};

/**
 * @brief Run all scenarios and check their statuses
 *
 * @retval true if all tests succeeds
 * @retval false if one test fails
 */
int main() {
    int  fnResult = EXIT_SUCCESS;
    bool testResult;
    int  i;

    i = 0;
    while (testFunctions[i] != NULL) {
        printf("Run test #%d\n", i);

        testResult = testFunctions[i]();
        if (testResult == true) {
            printf("Test #%d succeed\n", i);
        } else {
            printf("Test #%d failed !!\n", i);
            fnResult = EXIT_FAILURE;
        }

        i++;
    }

    if (fnResult == EXIT_SUCCESS) {
        printf("Test suite succeed\n");
    } else {
        printf("test suite failed\n");
    }

    return fnResult;
}

